//  Created by Daniel on 11/4/20.

import Foundation
import SwiftUI

open class ICommonViewModel: ObservableObject {
    @Published final var isAlertPresented: Bool = false
    @Published final var alertError: DomainError? = nil
    
    final func handleError(_ domainError: DomainError) {
        self.alertError = domainError
        self.isAlertPresented = true
    }
    
    final func onErrorAlertTap() {
        self.isAlertPresented = false
        self.alertError = nil
    }
}
