//  Created by Daniel on 11/4/20.

import Foundation
import SwiftUI

fileprivate struct ReminderListItemView: View {
    enum Style { case plain, removed, updated, added }
    
    let reminder: Reminder
    let style: Style
    
    var headerColor: Color {
        switch style {
        case .plain: return .black
        case .removed: return Color.red.opacity(0.7)
        case .updated: return Color.orange.opacity(0.7)
        case .added: return Color.green.opacity(0.7)
        }
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 2) {
            Text(reminder.title)
                .font(.headline)
                .fontWeight(.semibold)
                .foregroundColor(self.headerColor)
            Text(reminder.text)
                .font(.subheadline)
        }
    }
}

struct RemindersListView<ViewModel: IRemindersListViewModel>: View, ViewAlertDisplayable {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        NavigationView {
            List() {
                if viewModel.addingReminders.isEmpty == false {
                    VStack(alignment: .leading, spacing: 8) {
                        Text("Добавляются:")
                            .font(.title)
                        ForEach(viewModel.addingReminders, id: \.id) { reminder in
                            ReminderListItemView(reminder: reminder, style: .added)
                        }
                        .padding(.leading, 32)
                    }
    
                }
                if viewModel.updatingReminders.isEmpty == false {
                    VStack(alignment: .leading, spacing: 8) {
                        Text("Обновляются:")
                            .font(.title)
                        ForEach(viewModel.updatingReminders, id: \.id) { reminder in
                            ReminderListItemView(reminder: reminder, style: .updated)
                        }
                        .padding(.leading, 32)
                    }
    
                }
                if viewModel.removingReminders.isEmpty == false {
                    VStack(alignment: .leading, spacing: 8) {
                        Text("Удаляются:")
                            .font(.title)
                        ForEach(0 ..< viewModel.removingReminders.count) { index in
                            ReminderListItemView(reminder: viewModel.removingReminders[index],
                                                 style: .removed)
                        }
                        .padding(.leading, 32)
                    }
                }
                
                if viewModel.reminders.isEmpty == false {
                    VStack(alignment: .leading, spacing: 8) {
                        let containtsOther = viewModel.addingReminders.isEmpty == false || viewModel.updatingReminders.isEmpty == false || viewModel.removingReminders.isEmpty == false
                        if containtsOther {
                            Text("Все:")
                                .font(.title)
                        }
                        ForEach(viewModel.reminders, id: \.id) { reminder in
                            ReminderListItemView(reminder: reminder, style: .plain)
                                .onTapGesture { self.viewModel.didTapReminder(reminder) }
                        }
                        .padding(.leading, 32)
                    }
                }
            }
            .navigationBarTitle("Напоминания")
            .navigationBarItems(trailing:
                                    Button("+")
                                        { self.viewModel.didTapAddButton() }
                                    .font(.title)
            )
        }
        .alert(isPresented: $viewModel.isAlertPresented, content: {
            self.makeAlert(for: viewModel.alertError,
                           onOkTap: { self.viewModel.onErrorAlertTap() })
        })
    }
}

#if DEBUG
struct RemindersListView_Previews: PreviewProvider {
    private final class PreviewViewModel: IRemindersListViewModel {
        override init() {
            super.init()
            self.addingReminders = [
                Reminder(id: UUID(),
                         title: "Добавляемый 1",
                         text: "Note for addeed 1",
                         targetDate: Date()),
                Reminder(id: UUID(),
                         title: "Добавляемый 2",
                         text: "Note for addeed 2",
                         targetDate: Date())]
            self.updatingReminders = [
                Reminder(id: UUID(),
                         title: "Как я сходил в гости",
                         text: "Увидел муху цикатуху",
                         targetDate: Date())
            ]
            
            self.removingReminders = [
                Reminder(id: UUID(),
                         title: "Запись о которой лучше никому не знать",
                         text: "Однажды я ...",
                         targetDate: Date())
            ]
            
            self.reminders = [
                Reminder(id: UUID(),
                         title: "Я забыл про напоминание",
                         text: "Напомналка для напомилналки",
                         targetDate: Date())
            ]
        }
    }
    
    static var previews: some View {
        RemindersListView(viewModel: PreviewViewModel())
    }
}
#endif
