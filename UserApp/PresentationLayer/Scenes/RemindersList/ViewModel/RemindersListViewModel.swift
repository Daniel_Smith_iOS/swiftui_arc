//  Created by Daniel on 11/4/20.

import Foundation
import Combine

extension AnyCancellable {
    func cancelled(by cancallable: inout AnyCancellable) {
        cancallable = self
    }
}

open class IRemindersListViewModel: ICommonViewModel {
    @Published var addingReminders: [Reminder] = []
    @Published var updatingReminders: [Reminder] = []
    @Published var removingReminders: [Reminder] = []
    @Published var reminders: [Reminder] = []
    
    func didTapAddButton() {}
    func didTapReminder(_ reminder: Reminder) {}
}

final class RemindersListViewModel: IRemindersListViewModel {
    private let remindersManager: IRemindersManager
    private let router: IRemindersListRouter
    private var remindersCancallable: AnyCancellable?
    
    private var willTryAddReminderCancallable = AnyCancellable {}
    private var didAddReminderCancallable = AnyCancellable {}
    
    private var willTryUpdateReminderCancallbe = AnyCancellable {}
    private var didUpdateReminderCancallable = AnyCancellable {}
    
    init(remindersManager: IRemindersManager,
         router: IRemindersListRouter) {
        self.remindersManager = remindersManager
        self.router = router
        super.init()
        self.subscribeToRemindersManager()
        self.fetchReminders()
    }
    
    override func didTapAddButton() {
        self.router.showEditReminder(nil)
    }
    
    override func didTapReminder(_ reminder: Reminder) {
        self.router.showEditReminder(reminder)
    }
    
    func fetchReminders() {
        self.remindersManager
            .fetch()
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .finished: return
                case .failure(let error):
                    self?.handleError(error)
                }
            }, receiveValue: { [weak self] reminders in
                guard let self = self else { return }
                self.reminders = reminders
                self.objectWillChange.send()
            })
            .cancelled(by: &self.willTryAddReminderCancallable)
    }
    
    private func subscribeToRemindersManager() {
        self.subscribeToAddingReminders()
        self.subscribeToAddedReminders()
        self.subscribeToUpdatingReminders()
        self.subscribeToUpdatedReminders()
        self.subscribeToRemovingReminders()
        self.subscribeToFailingRemovingReminders()
        self.subscribeToRemovedReminders()
    }
    
    private func subscribeToAddingReminders() {
        remindersCancallable = self.remindersManager
            .willTryAddReminder
            .sink(receiveValue: { [weak self] reminder in
                self?.addingReminders.append(reminder)
                self?.objectWillChange.send()
            })
    }
    
    private func subscribeToAddedReminders() {
        self.remindersManager
            .didAddReminder
            .sink(receiveValue: { [weak self] reminder in
                guard let self = self else { return }
                self.addingReminders.removeAll(where: { $0.id == reminder.id })
                self.reminders.insert(reminder, at: 0)
                self.objectWillChange.send()
            })
            .cancelled(by: &self.didAddReminderCancallable)
    }
    
    private func subscribeToUpdatingReminders() {
        self.remindersManager
            .willTryUpdateReminder
            .sink(receiveValue: { [weak self] reminder in
                self?.updatingReminders.append(reminder)
                self?.reminders.removeAll(where: { $0.id == reminder.id })
                self?.objectWillChange.send()
            })
            .cancelled(by: &self.willTryUpdateReminderCancallbe) // aka disposed(by: )
    }
    
    private func subscribeToUpdatedReminders() {
        self.remindersManager
            .didUpdateReminder
            .sink(receiveValue: { [weak self] reminder in
                guard let self = self else { return }
                self.updatingReminders.removeAll(where: { $0.id == reminder.id })
                self.reminders.insert(reminder, at: 0)
                self.objectWillChange.send()
            })
            .cancelled(by: &self.didUpdateReminderCancallable)
    }
    
    private func subscribeToRemovingReminders() {
        _ = self.remindersManager
            .willTryRemoveReminder
            .sink(receiveValue: { [weak self] reminder in
                self?.removingReminders.append(reminder)
                self?.reminders.removeAll(where: { $0.id == reminder.id })
            })
    }
    
    private func subscribeToFailingRemovingReminders() {
        _ = self.remindersManager
            .failedRemoveReminder
            .sink(receiveValue: { [weak self] reminder in
                self?.removingReminders.removeAll(where: { $0.id == reminder.id })
                self?.reminders.append(reminder)
            })
    }
    
    private func subscribeToRemovedReminders() {
        _ = self.remindersManager
            .didUpdateReminder
            .sink(receiveValue: { [weak self] reminder in
                guard let self = self else { return }
                self.removingReminders.removeAll(where: { $0.id == reminder.id })
            })
    }
}
