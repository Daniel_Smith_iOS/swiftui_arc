//  Created by Daniel on 11/5/20.

import Foundation
import UIKit

protocol IRemindersListRouter: IEditReminderRoutable {}

final class RemindersListRouter: IRemindersListRouter {
    weak var hostViewController: UIViewController?
}
