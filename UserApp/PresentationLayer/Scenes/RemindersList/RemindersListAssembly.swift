//  Created by Daniel on 11/5/20.

import Foundation
import UIKit
import SwiftUI

struct RemindersListAssembly {
    
    static func makeModule() -> UIViewController {
        let router = RemindersListRouter()
        let viewModel = RemindersListViewModel(remindersManager: Factory.shared.find(),
                                               router: router)
        let viewController =  UIHostingController(rootView: RemindersListView(viewModel: viewModel))
        router.hostViewController = viewController
        return viewController
    }
}
