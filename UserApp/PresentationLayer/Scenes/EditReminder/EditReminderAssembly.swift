//  Created by Daniel on 11/5/20.

import Foundation
import UIKit
import SwiftUI

protocol IClosableRouter: IRouter {
    func closeModule()
}

extension IClosableRouter {
    func closeModule() {
        hostViewController?.dismiss(animated: true, completion: nil)
    }
}

protocol IEditReminderRouter: IClosableRouter {}

final class EditReminderRouter: IEditReminderRouter {
    weak var hostViewController: UIViewController?
}

struct EditReminderAssembly {
    
    static func makeModule(reminder: Reminder?) -> UIViewController {
        let router = EditReminderRouter()
        let viewModel = EditReminderViewModel(remindersManager: Factory.shared.find(),
                                              router: router,
                                              reminder: reminder)
        let viewController = UIHostingController(rootView: EditReminderView(viewModel: viewModel))
        router.hostViewController = viewController
        return viewController
    }
}
