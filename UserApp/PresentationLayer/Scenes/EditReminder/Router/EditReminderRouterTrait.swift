//  Created by Daniel on 11/5/20.

import Foundation
import UIKit

protocol IRouter: AnyObject {
    var hostViewController: UIViewController? { get set }
}

protocol IEditReminderRoutable: IRouter {
    func showEditReminder(_ reminder: Reminder?)
}

extension IEditReminderRoutable {
    func showEditReminder(_ reminder: Reminder?) {
        let viewController = EditReminderAssembly.makeModule(reminder: reminder)
        self.hostViewController?.present(viewController, animated: true, completion: nil)
    }
}
