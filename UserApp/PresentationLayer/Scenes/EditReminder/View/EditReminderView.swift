//  Created by Daniel on 11/4/20.

import Foundation
import SwiftUI

protocol ViewAlertDisplayable {
    func makeAlert(for error: DomainError?,
                   onOkTap: (() -> Void)?) -> Alert
}

extension ViewAlertDisplayable {
    
    func makeAlert(for error: DomainError?,
                   onOkTap: (() -> Void)?) -> Alert {
        Alert(title: Text("Error"),
              message: Text(error?.title ?? "Unknown"),
              dismissButton: .default(Text("OK"), action: onOkTap))
    }
}
struct EditReminderView<ViewModel: IEditReminderViewModel>: View, ViewAlertDisplayable {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack {
            TextField("Введите название", text: $viewModel.title)
            TextField("Заметки", text: $viewModel.noteText)
            Button(viewModel.doneButtonTitle,
                   action: { self.viewModel.didTapDoneButton() })
        }
        .padding()
        .alert(isPresented: $viewModel.isAlertPresented, content: {
            self.makeAlert(for: viewModel.alertError,
                           onOkTap: { self.viewModel.onErrorAlertTap() })
        })
    }
}

#if DEBUG
struct EditReminderView_Previews: PreviewProvider {
    private final class PreviewViewModel: IEditReminderViewModel {
        override init() {
            super.init()
            self.doneButtonTitle = "Добавить"
        }
    }
    
    static var previews: some View {
        EditReminderView(viewModel: PreviewViewModel())
    }
}
#endif
