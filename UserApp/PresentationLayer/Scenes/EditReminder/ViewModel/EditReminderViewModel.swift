//  Created by Daniel on 11/4/20.

import Foundation
import Combine

open class IEditReminderViewModel: ICommonViewModel {
    @Published final var title = ""
    @Published final var noteText = ""
    @Published final var doneButtonTitle = ""
    
    open func didTapDoneButton() {}
}

final class EditReminderViewModel: IEditReminderViewModel {
    private enum UseCase {
        case update(Reminder)
        case add
        
        init(_ reminder: Reminder?) {
            if let reminder = reminder {
                self = .update(reminder)
            } else {
                self = .add
            }
        }
    }
    
    private let remindersManager: IRemindersManager
    private let router: IEditReminderRouter
    private let useCase: UseCase
    
    init(remindersManager: IRemindersManager,
         router: EditReminderRouter,
         reminder: Reminder? = nil) {
        self.remindersManager = remindersManager
        self.router = router
        self.useCase = UseCase(reminder)
        super.init()
        self.prepareView()
    }
    
    override func didTapDoneButton() {
        switch self.useCase {
        case .update(let reminder):
            self.updateReminder(reminder)
        case .add:
            self.addReminder()
        }
        self.router.closeModule()
    }
    
    private func prepareView() {
        switch self.useCase {
        case .update(let reminder):
            self.title = reminder.title
            self.noteText = reminder.text
            self.doneButtonTitle = "Обновить"
        case .add:
            self.doneButtonTitle = "Добавить"
        }
    }
    
    private func updateReminder(_ reminder: Reminder) {
        let newReminder = Reminder(id: reminder.id,
                                   title: self.title,
                                   text: self.noteText,
                                   targetDate: Date())
        self.remindersManager.update(newReminder) { [weak self] in self?.handleError($0) }
    }
    
    private func addReminder() {
        let reminder = Reminder(id: UUID(),
                                title: self.title,
                                text: self.noteText,
                                targetDate: Date())
        self.remindersManager.add(reminder) { [weak self] in self?.handleError($0) }
    }
}
