//  Created by Daniel on 11/4/20.

import Foundation

struct Reminder {
    let id: UUID
    var title: String
    var text: String
    var targetDate: Date
}
