//  Created by Daniel on 11/4/20.

import Foundation

struct DomainError: Error {
    enum Source {
        case network, storage
    }
    let title: String
    let source: Source
}
