//  Created by Daniel on 11/4/20.

import Foundation
import Combine

protocol IRemindersManager: AnyObject {
    var willTryAddReminder: AnyPublisher<Reminder, Never> { get }
    var didAddReminder: AnyPublisher<Reminder, Never> { get }
    var failedAddReminder: AnyPublisher<Reminder, Never> { get }
    
    var willTryRemoveReminder: AnyPublisher<Reminder, Never> { get }
    var didRemoveReminder: AnyPublisher<Reminder, Never> { get }
    var failedRemoveReminder: AnyPublisher<Reminder, Never> { get }
    
    var willTryUpdateReminder: AnyPublisher<Reminder, Never> { get }
    var didUpdateReminder: AnyPublisher<Reminder, Never> { get }
    var failedUpdateReminder: AnyPublisher<Reminder, Never> { get }
    
    
    func add(_ reminder: Reminder,
             onError: ((DomainError) -> Void)?)
    func remove(_ reminder: Reminder,
                onError: ((DomainError) -> Void)?)
    func update(_ reminder: Reminder,
                onError: ((DomainError) -> Void)?)
    func fetch() -> AnyPublisher<[Reminder], DomainError>
}

final class RemindersManager: IRemindersManager {
    lazy var willTryAddReminder: AnyPublisher<Reminder, Never> = self.willTryAddReminderPublisher.share().eraseToAnyPublisher()
    lazy var didAddReminder: AnyPublisher<Reminder, Never> = self.didAddReminderPublisher.share().eraseToAnyPublisher()
    lazy var failedAddReminder: AnyPublisher<Reminder, Never> = self.failedAddReminderPublisher.share().eraseToAnyPublisher()
    
    lazy var willTryRemoveReminder: AnyPublisher<Reminder, Never> = self.willTryRemoveReminderPublisher.share().eraseToAnyPublisher()
    lazy var didRemoveReminder: AnyPublisher<Reminder, Never> = self.didRemoveReminderPublisher.share().eraseToAnyPublisher()
    lazy var failedRemoveReminder: AnyPublisher<Reminder, Never> = self.failedRemoveReminderPublisher.share().eraseToAnyPublisher()
    
    lazy var willTryUpdateReminder: AnyPublisher<Reminder, Never> = self.willTryUpdateReminderPublisher.share().eraseToAnyPublisher()
    lazy var didUpdateReminder: AnyPublisher<Reminder, Never> = self.didUpdateReminderPublisher.share().eraseToAnyPublisher()
    lazy var failedUpdateReminder: AnyPublisher<Reminder, Never> = self.failedUpdateReminderPublisher.share().eraseToAnyPublisher()
    
    private let remindersSubject = CurrentValueSubject<[Reminder], Never>([])
    private let willTryAddReminderPublisher = PassthroughSubject<Reminder, Never>()
    private let didAddReminderPublisher = PassthroughSubject<Reminder, Never>()
    private let failedAddReminderPublisher = PassthroughSubject<Reminder, Never>()
    
    private let willTryRemoveReminderPublisher = PassthroughSubject<Reminder, Never>()
    private let didRemoveReminderPublisher = PassthroughSubject<Reminder, Never>()
    private let failedRemoveReminderPublisher = PassthroughSubject<Reminder, Never>()
    
    private let willTryUpdateReminderPublisher = PassthroughSubject<Reminder, Never>()
    private let didUpdateReminderPublisher = PassthroughSubject<Reminder, Never>()
    private let failedUpdateReminderPublisher = PassthroughSubject<Reminder, Never>()
    
    private var operationCounter = 0
    
    func add(_ reminder: Reminder,
             onError: ((DomainError) -> Void)?) {
        self.operationCounter += 1
        
        self.willTryAddReminderPublisher.send(reminder)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let error = self.makeErrorIfNeeded(title: "Ошибка API добавления напоминания") {
                onError?(error)
                self.failedAddReminderPublisher.send(reminder)
            } else {
                self.didAddReminderPublisher.send(reminder)
            }
        }
    }
    
    func remove(_ reminder: Reminder,
                onError: ((DomainError) -> Void)?) {
        self.operationCounter += 1
        
        self.willTryRemoveReminderPublisher.send(reminder)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let error = self.makeErrorIfNeeded(title: "Ошибка API удаления напоминания") {
                onError?(error)
                self.failedRemoveReminderPublisher.send(reminder)
            } else {
                self.didRemoveReminderPublisher.send(reminder)
            }
        }
    }
    
    func update(_ reminder: Reminder,
                onError: ((DomainError) -> Void)?) {
        self.operationCounter += 1
        
        self.willTryUpdateReminderPublisher.send(reminder)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let error = self.makeErrorIfNeeded(title: "Ошибка API обновления напоминания") {
                onError?(error)
                self.failedUpdateReminderPublisher.send(reminder)
            } else {
                self.didUpdateReminderPublisher.send(reminder)
            }
        }
    }
    
    func load(onError: ((DomainError) -> Void)?) {
        self.operationCounter += 1
        if let error = self.makeErrorIfNeeded(title: "Ошибка API загрузки напоминаний") {
            onError?(error)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                let titles = ["Купить яблоки", "Обновить архитектуру", "Изучить питон", "Понять как рабоать в SwiftUI", "Любой тайтл"]
                let subtitles = ["The FetchedResults collection type represents the results of performing a fetch request. Internally, it may use strategies such as batching and transparent futures to minimize memory use and I/O",
                                 "Define interactions from taps, clicks, and swipes to fine-grained gestures",
                                 "SwiftUI implements many data management types, like State and Binding, as Swift property wrappers. ",
                                 "The property gains the behavior specified by the wrapper"]
                let testData = Array((0...10)).map {
                    Reminder(id: UUID(),
                             title: titles[$0 % titles.count],
                             text: subtitles[$0 % subtitles.count],
                             targetDate: Date().addingTimeInterval(TimeInterval(-3600 * $0)))
                }
                self.remindersSubject.send(testData)
            }
        }
    }
    func fetch() -> AnyPublisher<[Reminder], DomainError> {
        self.operationCounter += 1
        
        return Future<[Reminder], DomainError> { [unowned self] promise in
            if let error = self.makeErrorIfNeeded(title: "Ошибка API загрузки напоминаний") {
                promise(.failure(error))
            } else {
                let titles = ["Купить яблоки", "Обновить архитектуру", "Изучить питон", "Понять как рабоать в SwiftUI", "Любой тайтл"]
                let subtitles = ["The FetchedResults collection type represents the results of performing a fetch request. Internally, it may use strategies such as batching and transparent futures to minimize memory use and I/O",
                                 "Define interactions from taps, clicks, and swipes to fine-grained gestures",
                                 "SwiftUI implements many data management types, like State and Binding, as Swift property wrappers. ",
                                 "The property gains the behavior specified by the wrapper"]
                let testData = Array((0...10)).map {
                    Reminder(id: UUID(),
                             title: titles[$0 % titles.count],
                             text: subtitles[$0 % subtitles.count],
                             targetDate: Date().addingTimeInterval(TimeInterval(-3600 * $0)))
                }
                promise(.success(testData))
            }
        }
        .eraseToAnyPublisher()
        

    }
    
    private func makeErrorIfNeeded(title: String) -> DomainError? {
         operationCounter % 10 == 0 ? DomainError(title: title, source: .network) : nil
    }
}
