//  Created by Daniel on 11/4/20.

import Foundation

final class Factory {
    static let shared = Factory()
    
    static var remindersManager: RemindersManager?
    
    func find() -> RemindersManager {
        if let instance = Factory.remindersManager {
            return instance
        }
        let instance = RemindersManager()
        Factory.remindersManager = instance
        return instance
    }
}
